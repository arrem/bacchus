# Bacchus

Bacchus is a bounded model checker supporting [ASCII AIGER files](http://fmv.jku.at/aiger/) written in the Rust
programming language, for a Computer-Aided Verification course at TU Vienna in the summer semester of 2020.

## Files

Since the project is quite small in code, we give a brief overview of each source file:

* `main.rs` - Contains the main function, reads the command line arguments and invokes the main logic.
* `lib.rs` - Contains the main logic, combining all the parts of the project together into a single unit.
* `parser.rs` - Parses `.aag` files into an object that can be easily processed.
* `sat.rs` - SAT solver interface, offers DIMACS generation from a parsed `.aag` file, and interfaces with the
(modified) [MiniSat](http://minisat.se/) solver.
* `examples/` - Contains a few examples from the AIGER homepage translated to the ASCII format.
