mod parser;
mod sat;

use std::error::Error;
use sat::DimacsGenerator;

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let aag_file = parser::parse(config.path)?;
    let generator = DimacsGenerator::new(aag_file);

    (0..=config.k).map(|s| {
        let dimacs = generator.generate_dimacs(s);
        println!("For k={}", s);
        sat::solver(&dimacs)
    }).collect::<Result<Vec<_>, _>>()?;

    Ok(())
}

pub struct Config {
    pub path: String,
    pub k: u32,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &'static str> {
        if args.len() < 2 {
            return Err("Expected two arguments.");
        }

        let path = args[1].clone();
        let k = args[2].parse::<u32>().map_err(|_| {
            "K could not be parsed as an integer value."
        })?;

        Ok(Config { path, k })
    }
}
