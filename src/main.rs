#[macro_use]
extern crate log;

use std::{env, process};

use bacchus::Config;

fn main() {
    env_logger::init();

    let args: Vec<String> = env::args().collect();
    let config = Config::new(&args).unwrap_or_else(|e| {
        error!("Problem parsing arguments: {}", e);
        process::exit(1);
    });

    trace!("Parsing {} for k={}", config.path, config.k);

    if let Err(e) = bacchus::run(config) {
        error!("{}", e);
        process::exit(1);
    }
}
