use std::fs;
use std::path::Path;
use std::error::Error;
use std::fmt;
use std::fmt::Formatter;
use log::{debug, trace};

/// A ParserError represents any error that might occur during parsing an .aag file. All errors are thrown with a
/// helpful message that describes what went wrong.
#[derive(Debug)]
struct ParserError {
    message: String
}

impl Error for ParserError { }

impl fmt::Display for ParserError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "Parser error: {}", self.message)
    }
}

impl ParserError {
    fn new<M: Into<String>>(message: M) -> Self {
        ParserError { message: message.into() }
    }
}

/// Literals are used to construct formulas describing a circuit. All literals consist of an identifier (1..`M`), where
/// `M` is the maximum variable index as specified in the AIGER file, and a boolean value indicating whether the literal
/// is positive.
/// Since AIGER files also support true/false constants, we encode these as literals as well, assigning them the
/// identifier 0 and a respective sign. The formula generator then asserts that the literal 0 is always set to true,
/// ensuring that false (which is the negated 0 literal) is always false.
#[derive(Clone, Copy)]
pub struct Literal {
    pub identifier: u32,
    pub positive: bool,
}

impl Literal {
    pub fn new(identifier: u32, positive: bool) -> Self {
        Literal { identifier, positive }
    }

    /// Creates a new literal from an AIGER integer specification. In AIGER, 0 represents the constant false,
    /// 1 represents the constant true, and for every n >= 2, `i = floor(n / 2)` is the identifier of the literal
    /// and `p := i % 2 == 0` is its polarity.
    ///
    /// # Examples
    ///
    /// ```
    /// let l1 = Literal::from_integer(1);
    ///
    /// // The integer 1 resolves to the always true constant.
    /// assert_eq!(l1.identifier, 0);
    /// assert_eq!(l1.positive, true);
    ///
    /// let l2 = Literal::from_integer(5);
    /// assert_eq!(l2.identifier, 2);
    /// assert_eq!(l2.positive, false);
    /// ```
    pub fn from_integer(integer: i32) -> Self {
        if integer == 0 || integer == 1 {
            Literal::new(0u32, integer % 2 == 1)
        } else {
            Literal::new((integer / 2) as u32, integer % 2 == 0)
        }
    }

    /// Creates a copy of this literal with the specified polarity. The identifier of the literal remains unchanged.
    pub fn with_polarity(&self, positive: bool) -> Literal {
        Literal { positive, ..*self }
    }

    /// Creates a copy of this literal with polarity opposite of the original literal's polarity. The identifier of
    /// the literal remains unchanged.
    pub fn negate(&self) -> Literal {
        Literal { positive: !self.positive, ..*self }
    }
}

impl fmt::Debug for Literal {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self)
    }
}

impl fmt::Display for Literal {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}x{}", if self.positive { "" } else { "-" }, self.identifier)
    }
}

/// Represents a D-Latch as used by the AIGER file format. Each latch is fully described by the variable which contains
/// its current value and a variable which contains its value in the next state (tick) of the simulation.
pub struct Latch {
    pub current_value: Literal,
    pub next_value: Literal,
}

impl Latch {
    pub fn new(current_value: Literal, next_value: Literal) -> Self {
        Latch { current_value, next_value }
    }
}

impl fmt::Debug for Latch {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self)
    }
}

impl fmt::Display for Latch {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{{current: {}, next:{}}}", self.current_value, self.next_value)
    }
}

/// Represents a logical AND-Gate as used by the AIGER file format. And gates are fully described by two variables which
/// contain the gate's inputs, and a third variable which contains its output. All the variables are assumed to be
/// updating in the same step, thus `out := in1 and in2` holds for every step.
pub struct AndGate {
    pub output: Literal,
    pub input1: Literal,
    pub input2: Literal,
}

impl AndGate {
    pub fn new(output: Literal, input1: Literal, input2: Literal) -> Self {
        AndGate { output, input1, input2 }
    }
}

impl fmt::Debug for AndGate {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self)
    }
}

impl fmt::Display for AndGate {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{{{} <- {} & {}}}", self.output, self.input1, self.input2)
    }
}

/// Represents a parsed AIGER file, containing all the information necessary to construct the circuit that the file is
/// describing, including the total variable count (without the true/false constant), the input literals, the latches,
/// the single output literal (since the BMC used here is only dealing with a single-output bit which is a bad state
/// detector) and the and gates.
pub struct AigerFile {
    pub variable_count: u32,
    pub inputs: Vec<Literal>,
    pub latches: Vec<Latch>,
    pub output: Literal,
    pub and_gates: Vec<AndGate>
}

impl AigerFile {
    pub fn new(variable_count: u32, inputs: Vec<Literal>, latches: Vec<Latch>, output: Literal, and_gates: Vec<AndGate>) -> Self {
        AigerFile { variable_count, inputs, latches, output, and_gates }
    }
}

/// Reads the contents of the given `.aag` file and produces a new `AigerFile` containing information about the circuit.
pub fn parse<P: AsRef<Path>>(path: P) -> Result<AigerFile, Box<dyn Error>> {
    let input = fs::read_to_string(path)?;
    let lines = input.lines().collect::<Vec<_>>();

    if lines.is_empty() {
        return Err(Box::new(ParserError::new("Expected aag header at start of file.")))
    }

    let header = lines[0];

    if !header.starts_with("aag") {
        return Err(Box::new(ParserError::new("Expected aag header at start of file.")))
    }

    let header_split = header.split_whitespace().collect::<Vec<_>>();

    if header_split.len() != 6 {
        return Err(Box::new(ParserError::new("aag file header should have 5 components.")))
    }

    let max_variable_index: u32 = header_split[1].parse()?;
    let input_wires: usize = header_split[2].parse()?;
    let latch_count: usize = header_split[3].parse()?;
    let output_wires: usize = header_split[4].parse()?;
    let and_gates: usize = header_split[5].parse()?;

    trace!(
        ".aag file header: {} variables, {} inputs, {} latches, {} outputs, {} and gates.",
        max_variable_index,
        input_wires,
        latch_count,
        output_wires,
        and_gates
    );

    let expected_length = input_wires + latch_count + output_wires + and_gates + 1;

    if lines.len() < expected_length {
        return Err(Box::new(ParserError::new("aag file terminated prematurely.")))
    }

    let inputs = lines.iter().skip(1).take(input_wires).map(|l| {
        l.parse().map(Literal::from_integer)
    }).collect::<Result<Vec<_>, _>>()?;

    debug!("Input literals: {:?}", inputs);

    let latches = lines.iter().skip(1 + input_wires).take(latch_count).map(|l| {
        let split = l.split_whitespace().collect::<Vec<_>>();

        split[0].parse::<i32>().and_then(|f| {
            split[1].parse::<i32>().map(|s| {
                Latch::new(Literal::from_integer(f), Literal::from_integer(s))
            })
        })
    }).collect::<Result<Vec<_>, _>>()?;

    debug!("Latches: {:?}", latches);

    let outputs = lines.iter().skip(1 + input_wires + latch_count).take(output_wires).map(|l| {
        l.parse().map(Literal::from_integer)
    }).collect::<Result<Vec<_>, _>>()?;

    debug!("Output literals: {:?}", outputs);

    if outputs.len() != 1 {
        return Err(Box::new(ParserError::new("aag file header should have only one output.")))
    }

    let and_gates = lines.iter().skip(1 + input_wires + latch_count + output_wires).take(and_gates).map(|l| {
        let split = l.split_whitespace().collect::<Vec<_>>();

        split[0].parse::<i32>().and_then(|o| {
            split[1].parse::<i32>().and_then(|i1| {
                split[2].parse::<i32>().map(|i2| {
                    AndGate::new(Literal::from_integer(o), Literal::from_integer(i1), Literal::from_integer(i2))
                })
            })
        })
    }).collect::<Result<Vec<_>, _>>()?;

    debug!("And gates: {:?}", and_gates);

    Ok(AigerFile::new(max_variable_index, inputs, latches, *outputs.first().unwrap(), and_gates))
}
