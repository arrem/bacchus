use std::process::{Command, Stdio};
use std::io::Write;
use std::error::Error;
use crate::parser::{AigerFile, Literal};
use std::fmt::{self, Display, Formatter};

struct Clause {
    literals: Vec<i32>
}

impl Clause {
    pub fn new(literals: Vec<i32>) -> Self {
        Clause { literals }
    }
}

impl Display for Clause {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{} 0", self.literals.iter().map(|i| i.to_string()).collect::<Vec<_>>().join(" "))
    }
}

pub struct DimacsGenerator {
    file: AigerFile
}

impl DimacsGenerator {
    pub fn new(file: AigerFile) -> Self {
        DimacsGenerator { file }
    }

    /// Converts the given AIGER file into a DIMACS file, encoding the circuit as a CNF formula. We chose not to use
    /// a two step approach where a propositional formula is generated and then converted into CNF through a Tseitin
    /// transformation for two reasons. Firstly, Tseitin transformation is a lot easier to implement recursively, and
    /// tests on the JVM have shown that this requires quite a large stack to be able to handle cases up to k = 30.
    /// Moreover, directly translating formulas into CNF by hand saves computation time and results in the final solver
    /// being a lot faster, by quite a big margin. Since the formulas we are dealing with contain no nested equivalence
    /// operations, we do not have an exponential blow-up that would benefit from a Tseitin transformation. All of our
    /// formulas are simple enough to be easily translatable by hand, which is the approach that we have opted for.
    ///
    /// To convert a circuit into a SAT instance, this function does the following:
    /// 1. It is asserted that in the initial step, all latch outputs are zero, as per the specification in the project
    /// description.
    /// 2. On each step, the value of an AND gate's output is the value of its inputs AND-ed together. The CNF
    /// transformation for this and the next step are outlined in the comments.
    /// 3. In step `k + 1`, the value of a latch's `current_value` variable is the value of its `next_value` variable
    /// in step `k`.
    /// 4. The safety property is encoded, that is, the bad state detector bit is 1 in any of the steps.
    /// 5. Since we opted to model the true/false constant as a variable and its negation respectively, we need to
    /// ensure that this variable has the value `true` throughout the simulation.
    /// 6. Lastly, the DIMACS header is computed. Since the clauses are aggregated in a collection, getting the number
    /// of clauses is trivial. To get the number of variables, we use `(N + 1) * (maxK + 1)`. The former `+ 1` resulting
    /// from the fact that true/false is modelled as a variable, and the latter due to the fact that we also run checks
    /// for k = 0.
    pub fn generate_dimacs(&self, k: u32) -> String {
        let mut clauses = Vec::new();

        // 1. Initial state latches:
        self.file.latches.iter()
            .map(|l| self.variable(&l.current_value.with_polarity(false), 0))
            .for_each(|l| {
                clauses.push(Clause::new(vec![l]))
            });

        // 2. And gate input output hooks:
        // (CNF) of A ↔ (B ∧ C)
        // (¬B ∨ ¬C ∨ A) ∧ (¬A ∨ B) ∧ (¬A ∨ C)
        (0..=k).flat_map(|s| {
            self.file.and_gates.iter().flat_map(move |g| {
                vec![
                    Clause::new(vec![self.variable(&g.input1.negate(), s), self.variable(&g.input2.negate(), s), self.variable(&g.output, s)]),
                    Clause::new(vec![self.variable(&g.output.negate(), s), self.variable(&g.input1, s)]),
                    Clause::new(vec![self.variable(&g.output.negate(), s), self.variable(&g.input2, s)])
                ]
            })
        }).for_each(|c| clauses.push(c));

        // 3. Latch current and next state hooks:
        // (CNF) of A ↔ B
        // (¬B ∨ A) ∧ (¬A ∨ B)
        (0..k).flat_map(|s| {
            self.file.latches.iter().flat_map(move |l| {
                vec![
                    Clause::new(vec![self.variable(&l.next_value.negate(), s), self.variable(&l.current_value, s + 1)]),
                    Clause::new(vec![self.variable(&l.current_value.negate(), s + 1), self.variable(&l.next_value, s)])
                ]
            })
        }).for_each(|c| clauses.push(c));

        // 4. Safety property:
        let safety = (0..=k).map(|s| self.variable(&self.file.output, s)).collect::<Vec<_>>();
        clauses.push(Clause::new(safety));

        // 5. True wire stays true:
        (0..=k)
            .map(|s| self.variable(&Literal::from_integer(1), s))
            .for_each(|l| {
                clauses.push(Clause::new(vec![l]))
            });

        format!("p cnf {} {}\n{}", (self.file.variable_count + 1) * (k + 1), clauses.len(), clauses.iter().map(|c| format!("{}", c)).collect::<Vec<_>>().join("\n"))
    }

    fn variable(&self, literal: &Literal, k: u32) -> i32 {
        let sign = if literal.positive { 1 } else { -1i32 };
        let identifier = 1 + literal.identifier + (self.file.variable_count + 1) * k;

        sign * (identifier as i32)
    }
}

/// Depending on the underlying OS, this function invokes MiniSAT with the generate proof option, pipes the given
/// DIMACS file into the child process' standard input and reads out the output of the solver.
pub fn solver(dimacs: &str) -> Result<(), Box<dyn Error>> {
    let mut command = if cfg!(target_os = "windows") {
        panic!("Windows is not supported at the moment.")
    } else {
        Command::new("./minisat/minisat")
    }
        .arg("-c")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()?;

    let stdin = command.stdin.as_mut().unwrap();
    stdin.write_all(dimacs.as_bytes())?;
    stdin.flush()?;

    let command = command.wait_with_output()?;
    let result = String::from_utf8(command.stdout)?;

    println!("{}", result.trim().lines().last().unwrap());

    Ok(())
}
